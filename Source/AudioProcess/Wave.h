/*
  ==============================================================================

    Wave.h
    Created: 30 Sep 2021 9:10:50pm
    Author:  migizo

  ==============================================================================
*/

#pragma once

class Wave {
public:
    virtual void update(float phase) = 0;
    virtual float get() = 0;
};