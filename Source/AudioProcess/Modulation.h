/*
  ==============================================================================

    Modulation.h
    Created: 30 Sep 2021 9:10:36pm
    Author:  migizo

  ==============================================================================
*/

#pragma once
#include "Wave.h"

class Modulation {
public:
    Modulation(Wave* wave, float _sampleRate = 44100);
    void setModulation(Wave* wave);
    float process(float inVal, float speed);
private:
    Wave* wave;
    float counter;
    float sampleRate;
};
