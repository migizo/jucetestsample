/*
  ==============================================================================

    Modulation.cpp
    Created: 30 Sep 2021 9:10:36pm
    Author:  migizo

  ==============================================================================
*/

#include "Modulation.h"
#include <math.h>

Modulation::Modulation(Wave* wave, float _sampleRate)
    : sampleRate(_sampleRate), counter(0)
{
    setModulation(wave);
}

void Modulation::setModulation(Wave* wave) {
    this->wave = wave;
}

float Modulation::process(float inVal, float speed) {
    wave->update(counter / sampleRate);
    counter = fmodf(counter + speed, sampleRate);

    return inVal * wave->get();
}
