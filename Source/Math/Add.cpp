/*
  ==============================================================================

    Add.cpp
    Created: 30 Sep 2021 6:03:59pm
    Author:  migizo

  ==============================================================================
*/

#include "Add.h"
int Add::calculate(int a, int b) {
    return a + b;
}
