/*
  =============================================================================

    MyTest.
    Created: 30 Sep 2021 5:38:02p
    Author:  migiz

  =============================================================================
*/

#pragma once
#include <JuceHeader.h>

class MyTest : public UnitTest {
 public:
  MyTest() : UnitTest("juce unit test", "category") {}
  
  // expect()はtrueが期待される式を書きます。
  void runTest() override {
    Logger::outputDebugString(juce::String("first test"));
    beginTest("Part 1");
    expect(true);
    expect(true);
    beginTest("Part 2");
    expect(true);
  }
};
