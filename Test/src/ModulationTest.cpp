#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../../Source/AudioProcess/Modulation.h"
#include "MockWave.h"

TEST(modulation, mockwaveTest)
{
  MockWave wave;

  // 期待される関数呼び出しの仕様(=Exception)を用意する
  // 今回の場合、update関数が最低でも一回呼ばれるのが期待されている
  EXPECT_CALL(wave, update(0)).Times(::testing::AtLeast(1));

  Modulation modulation(&wave);

  // [PASSED] テストには通るもののget()で返す値が明確に指定されていない旨が言及されます。
  modulation.process(0, 0);
  // modulation.process(1, 0);

  // [FAILED] 2回目でupdate()に渡されている値が0ではないのでテストに失敗する
  /*
    for (int i = 0; i < 2; i++)
    {
      modulation.process(0, 1);
    }
  */
}