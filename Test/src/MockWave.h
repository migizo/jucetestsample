#pragma once

#include "gmock/gmock.h"
#include "../../Source/AudioProcess/Wave.h"

class MockWave : public Wave
{
public:
  MOCK_METHOD1(update, void(float phase));
  MOCK_METHOD0(get, float());
};
