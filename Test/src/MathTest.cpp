#include "gtest/gtest.h"

#include "../../Source/Math/Add.h"

TEST(math, add)
{
  EXPECT_EQ(2, Add::calculate(1, 1));
  EXPECT_EQ(3, Add::calculate(2, 1));
}